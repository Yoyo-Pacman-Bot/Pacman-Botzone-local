# Pacman Botzone Local (Pakkuman Classroom)

## Link

[BotZone](http://botzone.org/)

## 介绍

这是一个失败计划的副产品。

只需要提供地图、玩家AI程序（Bot）、`config`文件，就可以模拟运行一盘`Pacman`（吃豆人）游戏（没有图形界面哦），生成`globalData`和`log`。

## 平台需求

*   Windows
*   .NET Framework 4.6.1 或更高

## 地图

Botzone交互中的`玩家输入Json`中的"requests"数组第零号元素内容作为Json文件。

## 玩家AI程序

就跟Botzone上的一样，但是需要编译成exe，且需要在编译时或在源程序代码中 define
`BOTZONE_ONLINE`，也可以使用提供的`g++Batch.bat`采用g++编译。

## config文件

模板 `config-template.txt`
```xml
<!-- 2016/05/09 -->
<!-- 所有的元素不能有空格、换行符、制表符 -->
<Working_Path> <!-- 之后所有路径相对于此 -->
<Map> <!-- 开局地图json文件 -->
<Log> <!-- 日志输出json文件夹, 每一盘输出独立的json 共有两种Log:
        1. *.requests-log.json 储存本盘输出给Bot的requests json对象（id和seed未定义，默认为map原值）
        2. *.visual-log.json 可供VisualLab直接读取的全盘比赛信息（暂只有必要信息）
        -->
[for i = 0 ~ 3]
<Bot-i_Path> <!-- Bot-i 独立文件夹，此Bot的以下所有文件放在此文件夹下
                  1.  可执行文件名称为 bot(.exe)
                  2.  全局变量文件为   gd.json
                  -->
<Bot-i_config-additional-turn> <!-- Bot-i 死亡/胜利后额外回合(用于learning)
                                    1: true; 0: false; -->
[end for]
<Repeat> <!-- 同一config的一局游戏的重复次数 -->
```

可以使用`configMaker`由地图批量生成config文件

## log

log有两种

`requests`: 最后一论的玩家输入的requests的公共部分
`visual`: 可以用`Pacman-Visual`（VisualLab，见项目文件）打开并可视化查看对局的json文件。

## Pacman-Visual (`VisualLab`)

吃豆人可视化工具，推荐使用`Firefox/Chrome`打开。