#include <string>


using namespace System;
using namespace System::Diagnostics;
using namespace System::IO;

void OutputHandler(Object^ /*sendingProcess*/,
    DataReceivedEventArgs^ outLine)
{
    // Collect the sort command output.
    if (!String::IsNullOrEmpty(outLine->Data))
    {
        Console::WriteLine(outLine->Data->ToString());
    }
}

int main(int argc, const char * argv[]) {
    Process^ process1 = gcnew Process();
    Process^ process2 = gcnew Process();
    try {
        process1->StartInfo->UseShellExecute = false;
        process1->StartInfo->FileName = "C:\\Users\\yo-yo\\Desktop\\PacClassTest_x86.exe";
        process1->StartInfo->CreateNoWindow = true;
        process1->StartInfo->RedirectStandardInput = true;
        process1->StartInfo->RedirectStandardOutput = true;
        process1->OutputDataReceived += gcnew DataReceivedEventHandler(OutputHandler);
        process1->Start();
        StreamWriter^ streamWriter1 = process1->StandardInput;
        process1->BeginOutputReadLine();
        process2->StartInfo->UseShellExecute = false;
        process2->StartInfo->FileName = "C:\\Users\\yo-yo\\Desktop\\PacClassTest_x86.exe";
        process2->StartInfo->CreateNoWindow = true;
        process2->StartInfo->RedirectStandardInput = true;
        process2->StartInfo->RedirectStandardOutput = true;
        process2->OutputDataReceived += gcnew DataReceivedEventHandler(OutputHandler);
        process2->Start();
        process2->BeginOutputReadLine();
        StreamWriter^ streamWriter2 = process2->StandardInput;

        String^ inputText = Console::ReadLine();
        streamWriter1->WriteLine(inputText);
        streamWriter1->Close();
        inputText = Console::ReadLine();
        streamWriter2->WriteLine(inputText);
        streamWriter2->Close();
        process1->WaitForExit();
        process2->WaitForExit();
    }

    catch (System::Exception^ e) {
        System::Console::WriteLine(e->Message);
    }

    return 0;
}