#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <stack>
#include <stdexcept>

#include <ctime>

// inner include
#include "jsoncpp/json.h"
#include "Bot.h"

// System::String -> std::string
void MarshalString(System::String ^ s, std::string& os) {
    using namespace System::Runtime::InteropServices;
    const char* chars =
        (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
    os = chars;
    Marshal::FreeHGlobal(System::IntPtr((void*)chars));
}

void MarshalString(System::String ^ s, std::wstring& os) {
    using namespace System::Runtime::InteropServices;
    const wchar_t* chars =
        (const wchar_t*)(Marshal::StringToHGlobalUni(s)).ToPointer();
    os = chars;
    Marshal::FreeHGlobal(System::IntPtr((void*)chars));
}

// 改编自Bot->Sample author: lln, Yoyo

// #define
// #define FIELD_MAX_HEIGHT 20
#define FIELD_MAX_HEIGHT 11
// #define FIELD_MAX_WIDTH 20
#define FIELD_MAX_WIDTH 11
#define MAX_GENERATOR_COUNT 4 // 每个象限1
#define MAX_PLAYER_COUNT 4
#define MAX_TURN 100

// 你也可以选用 using namespace std; 但是会污染命名空间
using std::string;
using std::swap;
using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::runtime_error;

namespace local_botzone {
    Json::Value rootData; // 所有的data globalData在此
    Json::Value rootRequests; // 所有requests
    Json::Value rootVisual; // for visualLab
    Json::Value rootOthers;
}


// 平台提供的吃豆人相关逻辑处理程序
namespace Pacman
{
    const time_t seed = time(0);
    const int dx[] = { 0, 1, 0, -1, 1, 1, -1, -1 }, dy[] = { -1, 0, 1, 0, -1, 1, 1, -1 };

    // PacBot : : path typedef
    typedef short GridDistanceType; // 用于fieldDistance[s_i][s_j][d_i][d_j]
    typedef char  GridPathType;		// 用于    fieldPath[s_i][s_j][d_i][d_j]

                                    // 枚举定义；使用枚举虽然会浪费空间（sizeof(GridContentType) == 4），但是计算机处理32位的数字效率更高

                                    // 每个格子可能变化的内容，会采用“或”逻辑进行组合
    enum GridContentType
    {
        empty = 0, // 其实不会用到
        player1 = 1, // 1号玩家
        player2 = 2, // 2号玩家
        player3 = 4, // 3号玩家
        player4 = 8, // 4号玩家
        playerMask = 1 | 2 | 4 | 8, // 用于检查有没有玩家等
        smallFruit = 16, // 小豆子
        largeFruit = 32 // 大豆子
    };

    // 用玩家ID换取格子上玩家的二进制位
    GridContentType playerID2Mask[] = { player1, player2, player3, player4 };
    string playerID2str[] = { "0", "1", "2", "3" };

    // 让枚举也可以用这些运算了（不加会编译错误）
    template<typename T>
    inline T operator |=(T &a, const T &b)
    {
        return a = static_cast<T>(static_cast<int>(a) | static_cast<int>(b));
    }
    template<typename T>
    inline T operator |(const T &a, const T &b)
    {
        return static_cast<T>(static_cast<int>(a) | static_cast<int>(b));
    }
    template<typename T>
    inline T operator &=(T &a, const T &b)
    {
        return a = static_cast<T>(static_cast<int>(a) & static_cast<int>(b));
    }
    template<typename T>
    inline T operator &(const T &a, const T &b)
    {
        return static_cast<T>(static_cast<int>(a) & static_cast<int>(b));
    }
    template<typename T>
    inline T operator ++(T &a)
    {
        return a = static_cast<T>(static_cast<int>(a) + 1);
    }
    template<typename T>
    inline T operator ~(const T &a)
    {
        return static_cast<T>(~static_cast<int>(a));
    }

    // 每个格子固定的东西，会采用“或”逻辑进行组合
    enum GridStaticType
    {
        emptyWall = 0, // 其实不会用到
        wallNorth = 1, // 北墙（纵坐标减少的方向）
        wallEast = 2, // 东墙（横坐标增加的方向）
        wallSouth = 4, // 南墙（纵坐标增加的方向）
        wallWest = 8, // 西墙（横坐标减少的方向）
        generator = 16 // 豆子产生器
    };

    // 用移动方向换取这个方向上阻挡着的墙的二进制位
    GridStaticType direction2OpposingWall[] = { wallNorth, wallEast, wallSouth, wallWest };

    // 方向，可以代入dx、dy数组，同时也可以作为玩家的动作
    enum Direction
    {
        stay = -1,
        up = 0,
        right = 1,
        down = 2,
        left = 3,
        // 下面的这几个只是为了产生器程序方便，不会实际用到
        ur = 4, // 右上
        dr = 5, // 右下
        dl = 6, // 左下
        ul = 7 // 左上
    };

    // 场地上带有坐标的物件
    struct FieldProp
    {
        int row, col;
    };

    // 场地上的玩家
    struct Player : FieldProp
    {
        int strength;
        int powerUpLeft;
        bool dead;
    };

    // 回合新产生的豆子的坐标
    struct NewFruits
    {
        FieldProp newFruits[MAX_GENERATOR_COUNT * 8];
        int newFruitCount;
    } newFruits[MAX_TURN];
    int newFruitsCount = 0;

    // 状态转移记录结构
    struct TurnStateTransfer
    {
        enum StatusChange // 可组合
        {
            none = 0,
            ateSmall = 1,
            ateLarge = 2,
            powerUpCancel = 4,
            die = 8,
            error = 16
        };

        // 玩家选定的动作
        Direction actions[MAX_PLAYER_COUNT];

        // 此回合该玩家的状态变化
        StatusChange change[MAX_PLAYER_COUNT];

        // 此回合该玩家的力量变化
        int strengthDelta[MAX_PLAYER_COUNT];
    };

    // 游戏主要逻辑处理类，包括输入输出、回合演算、状态转移，全局唯一
    class GameField
    {
    private:
        // 为了方便，大多数属性都不是private的

        // 记录每回合的变化（栈）
        TurnStateTransfer backtrack[MAX_TURN];

        // 这个对象是否已经创建
        static bool constructed;

    public:
        // Yoyo: 一堆东西
        /*Json::Value information_of_field;*/ // Created by Harold_Lee, to keep track of the field information
                                              // deleted by Yoyo: changed to Json::Value local_botzone::rootRequests
                                          // 场地的长和宽
        int height, width;
        int generatorCount;
        int GENERATOR_INTERVAL, LARGE_FRUIT_DURATION, LARGE_FRUIT_ENHANCEMENT;

        // 场地格子固定的内容
        GridStaticType fieldStatic[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];

        // 场地格子会变化的内容
        GridContentType fieldContent[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];

        // PacBot : : path Grid declare
        GridDistanceType fieldDistance[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];
        GridPathType     fieldPath[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];

        int generatorTurnLeft; // 多少回合后产生豆子
        int aliveCount; // 有多少玩家存活
        int smallFruitCount;
        int turnID;
        FieldProp generators[MAX_GENERATOR_COUNT]; // 有哪些豆子产生器
        Player players[MAX_PLAYER_COUNT]; // 有哪些玩家

                                          // 玩家选定的动作
        Direction actions[MAX_PLAYER_COUNT];

        // 恢复到上次场地状态。可以一路恢复到最开始。
        // 恢复失败（没有状态可恢复）返回false
        bool PopState()
        {
            if (turnID <= 0)
                return false;

            const TurnStateTransfer &bt = backtrack[--turnID];
            int i, _;

            // 倒着来恢复状态

            for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                Player &_p = players[_];
                GridContentType &content = fieldContent[_p.row][_p.col];
                TurnStateTransfer::StatusChange change = bt.change[_];

                if (!_p.dead)
                {
                    // 5. 大豆回合恢复
                    if (_p.powerUpLeft || change & TurnStateTransfer::powerUpCancel)
                        _p.powerUpLeft++;

                    // 4. 吐出豆子
                    if (change & TurnStateTransfer::ateSmall)
                    {
                        content |= smallFruit;
                        smallFruitCount++;
                    }
                    else if (change & TurnStateTransfer::ateLarge)
                    {
                        content |= largeFruit;
                        _p.powerUpLeft -= LARGE_FRUIT_DURATION;
                    }
                }

                // 2. 魂兮归来
                if (change & TurnStateTransfer::die)
                {
                    _p.dead = false;
                    aliveCount++;
                    content |= playerID2Mask[_];
                }

                // 1. 移形换影
                if (!_p.dead && bt.actions[_] != stay)
                {
                    fieldContent[_p.row][_p.col] &= ~playerID2Mask[_];
                    _p.row = (_p.row - dy[bt.actions[_]] + height) % height;
                    _p.col = (_p.col - dx[bt.actions[_]] + width) % width;
                    fieldContent[_p.row][_p.col] |= playerID2Mask[_];
                }

                // 0. 救赎不合法的灵魂
                if (change & TurnStateTransfer::error)
                {
                    _p.dead = false;
                    aliveCount++;
                    content |= playerID2Mask[_];
                }

                // *. 恢复力量
                if (!_p.dead)
                    _p.strength -= bt.strengthDelta[_];
            }

            // 3. 收回豆子
            if (generatorTurnLeft == GENERATOR_INTERVAL)
            {
                generatorTurnLeft = 1;
                NewFruits &fruits = newFruits[--newFruitsCount];
                for (i = 0; i < fruits.newFruitCount; i++)
                {
                    fieldContent[fruits.newFruits[i].row][fruits.newFruits[i].col] &= ~smallFruit;
                    smallFruitCount--;
                }
            }
            else
                generatorTurnLeft++;

            return true;
        }

        // 判断指定玩家向指定方向移动是不是合法的（没有撞墙）
        inline bool ActionValid(int playerID, Direction &dir) const
        {
            if (dir == stay)
                return true;
            const Player &p = players[playerID];
            const GridStaticType &s = fieldStatic[p.row][p.col];
            return dir >= -1 && dir < 4 && !(s & direction2OpposingWall[dir]);
        }

        // 在向actions写入玩家动作后，演算下一回合局面，并记录之前所有的场地状态，可供日后恢复。
        // 是终局的话就返回false
        bool NextTurn()
        {
            int _, i, j;

            TurnStateTransfer &bt = backtrack[turnID];
            memset(&bt, 0, sizeof(bt));

            // 0. 杀死不合法输入
            for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                Player &p = players[_];
                if (!p.dead)
                {
                    Direction &action = actions[_];
                    if (action == stay)
                        continue;

                    if (!ActionValid(_, action))
                    {
                        bt.strengthDelta[_] += -p.strength;
                        bt.change[_] = TurnStateTransfer::error;
                        fieldContent[p.row][p.col] &= ~playerID2Mask[_];
                        p.strength = 0;
                        p.dead = true;
                        aliveCount--;
                    }
                    else
                    {
                        // 遇到比自己强♂壮的玩家是不能前进的
                        GridContentType target = fieldContent
                            [(p.row + dy[action] + height) % height]
                        [(p.col + dx[action] + width) % width];
                        if (target & playerMask)
                            for (i = 0; i < MAX_PLAYER_COUNT; i++)
                                if (target & playerID2Mask[i] && players[i].strength > p.strength)
                                    action = stay;
                    }
                }
            }

            // 1. 位置变化
            for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                Player &_p = players[_];
                if (_p.dead)
                    continue;

                bt.actions[_] = actions[_];

                if (actions[_] == stay)
                    continue;

                // 移动
                fieldContent[_p.row][_p.col] &= ~playerID2Mask[_];
                _p.row = (_p.row + dy[actions[_]] + height) % height;
                _p.col = (_p.col + dx[actions[_]] + width) % width;
                fieldContent[_p.row][_p.col] |= playerID2Mask[_];
            }

            // 2. 玩家互殴
            for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                Player &_p = players[_];
                if (_p.dead)
                    continue;

                // 判断是否有玩家在一起
                int player, containedCount = 0;
                int containedPlayers[MAX_PLAYER_COUNT];
                for (player = 0; player < MAX_PLAYER_COUNT; player++)
                    if (fieldContent[_p.row][_p.col] & playerID2Mask[player])
                        containedPlayers[containedCount++] = player;

                if (containedCount > 1)
                {
                    // NAIVE
                    for (i = 0; i < containedCount; i++)
                        for (j = 0; j < containedCount - i - 1; j++)
                            if (players[containedPlayers[j]].strength < players[containedPlayers[j + 1]].strength)
                                swap(containedPlayers[j], containedPlayers[j + 1]);

                    int begin;
                    for (begin = 1; begin < containedCount; begin++)
                        if (players[containedPlayers[begin - 1]].strength > players[containedPlayers[begin]].strength)
                            break;

                    // 这些玩家将会被杀死
                    int lootedStrength = 0;
                    for (i = begin; i < containedCount; i++)
                    {
                        int id = containedPlayers[i];
                        Player &p = players[id];

                        // 从格子上移走
                        fieldContent[p.row][p.col] &= ~playerID2Mask[id];
                        p.dead = true;
                        int drop = p.strength / 2;
                        bt.strengthDelta[id] += -drop;
                        bt.change[id] |= TurnStateTransfer::die;
                        lootedStrength += drop;
                        p.strength -= drop;
                        aliveCount--;
                    }

                    // 分配给其他玩家
                    int inc = lootedStrength / begin;
                    for (i = 0; i < begin; i++)
                    {
                        int id = containedPlayers[i];
                        Player &p = players[id];
                        bt.strengthDelta[id] += inc;
                        p.strength += inc;
                    }
                }
            }

            // 3. 产生豆子
            if (--generatorTurnLeft == 0)
            {
                generatorTurnLeft = GENERATOR_INTERVAL;
                NewFruits &fruits = newFruits[newFruitsCount++];
                fruits.newFruitCount = 0;
                for (i = 0; i < generatorCount; i++)
                    for (Direction d = up; d < 8; ++d)
                    {
                        // 取余，穿过场地边界
                        int r = (generators[i].row + dy[d] + height) % height, c = (generators[i].col + dx[d] + width) % width;
                        if (fieldStatic[r][c] & generator || fieldContent[r][c] & (smallFruit | largeFruit))
                            continue;
                        fieldContent[r][c] |= smallFruit;
                        fruits.newFruits[fruits.newFruitCount].row = r;
                        fruits.newFruits[fruits.newFruitCount++].col = c;
                        smallFruitCount++;
                    }
            }

            // 4. 吃掉豆子
            for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                Player &_p = players[_];
                if (_p.dead)
                    continue;

                GridContentType &content = fieldContent[_p.row][_p.col];

                // 只有在格子上只有自己的时候才能吃掉豆子
                if (content & playerMask & ~playerID2Mask[_])
                    continue;

                if (content & smallFruit)
                {
                    content &= ~smallFruit;
                    _p.strength++;
                    bt.strengthDelta[_]++;
                    smallFruitCount--;
                    bt.change[_] |= TurnStateTransfer::ateSmall;
                }
                else if (content & largeFruit)
                {
                    content &= ~largeFruit;
                    if (_p.powerUpLeft == 0)
                    {
                        _p.strength += LARGE_FRUIT_ENHANCEMENT;
                        bt.strengthDelta[_] += LARGE_FRUIT_ENHANCEMENT;
                    }
                    _p.powerUpLeft += LARGE_FRUIT_DURATION;
                    bt.change[_] |= TurnStateTransfer::ateLarge;
                }
            }

            // 5. 大豆回合减少
            for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                Player &_p = players[_];
                if (_p.dead)
                    continue;

                if (_p.powerUpLeft > 0 && --_p.powerUpLeft == 0)
                {
                    _p.strength -= LARGE_FRUIT_ENHANCEMENT;
                    bt.change[_] |= TurnStateTransfer::powerUpCancel;
                    bt.strengthDelta[_] += -LARGE_FRUIT_ENHANCEMENT;
                }
            }

            ++turnID;

            // 是否只剩一人？
            if (aliveCount <= 1)
            {
                for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
                    if (!players[_].dead)
                    {
                        bt.strengthDelta[_] += smallFruitCount;
                        players[_].strength += smallFruitCount;
                    }
                return false;
            }

            // 是否回合超限？
            if (turnID >= 100)
                return false;

            return true;
        }

        // 改改改！！！
        int ReadMap(std::string jsonFilePath)
        {
            string str /*, chunk*/;

            std::ifstream fin; fin.open(jsonFilePath, std::ios::in);
            fin >> str;
            fin.close();

            //std::ios::sync_with_stdio(false); //ω\\)
            //getline(cin, str);

            Json::Reader reader;
            Json::Value field /*input*/;
            reader.parse(str, field /*input*/);

            local_botzone::rootRequests.clear();
            local_botzone::rootRequests[(Json::Value::UInt) 0] = field /*input*/; // Harold Lee, 将系统给的Input保存下来
                                                    // changed by Yoyo: changed to Json::Value local_botzone::rootRequests
            local_botzone::rootData.clear();  // 清空
            local_botzone::rootOthers.clear(); // 清空
            local_botzone::rootVisual.clear();
            /*int len = input["requests"].size();*/

            // 读取场地静态状况
            Json::Value /*field = input["requests"][(Json::Value::UInt) 0],*/
                staticField = field["static"], // 墙面和产生器
                contentField = field["content"]; // 豆子和玩家
            height = field["height"].asInt();
            width = field["width"].asInt();
            LARGE_FRUIT_DURATION = field["LARGE_FRUIT_DURATION"].asInt();
            LARGE_FRUIT_ENHANCEMENT = field["LARGE_FRUIT_ENHANCEMENT"].asInt();
            generatorTurnLeft = GENERATOR_INTERVAL = field["GENERATOR_INTERVAL"].asInt();

            PrepareInitialField(staticField, contentField);

            // 根据历史恢复局面
            /* for (int i = 1; i < len; i++)
            {
                Json::Value req = input["requests"][i];
                for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
                    if (!players[_].dead)
                        actions[_] = (Direction)req[playerID2str[_]]["action"].asInt();
                NextTurn();
            } */

            return 0;
        }

        // 根据 static 和 content 数组准备场地的初始状况
        void PrepareInitialField(const Json::Value &staticField, const Json::Value &contentField)
        {
            int r, c, gid = 0;
            generatorCount = 0;
            aliveCount = 0;
            smallFruitCount = 0;
            generatorTurnLeft = GENERATOR_INTERVAL;
            for (r = 0; r < height; r++)
                for (c = 0; c < width; c++)
                {
                    GridContentType &content = fieldContent[r][c] = (GridContentType)contentField[r][c].asInt();
                    GridStaticType &s = fieldStatic[r][c] = (GridStaticType)staticField[r][c].asInt();
                    if (s & generator)
                    {
                        generators[gid].row = r;
                        generators[gid++].col = c;
                        generatorCount++;
                    }
                    if (content & smallFruit)
                        smallFruitCount++;
                    for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
                        if (content & playerID2Mask[_])
                        {
                            Player &p = players[_];
                            p.col = c;
                            p.row = r;
                            p.powerUpLeft = 0;
                            p.strength = 1;
                            p.dead = false;
                            aliveCount++;
                        }
                }
        }

        // 完成决策，输出结果。
        // action 表示本回合的移动方向，stay 为不移动
        // tauntText 表示想要叫嚣的言语，可以是任意字符串，除了显示在屏幕上不会有任何作用，留空表示不叫嚣
        // data 表示自己想存储供下一回合使用的数据，留空表示删除
        // globalData 表示自己想存储供以后使用的数据（替换），这个数据可以跨对局使用，会一直绑定在这个 Bot 上，留空表示删除
        void WriteOutput(Direction action, string tauntText = "", string data = "", string globalData = "") const
        {
            Json::Value ret;
            ret["response"]["action"] = action;
            ret["response"]["tauntText"] = tauntText;
            ret["data"] = data;
            ret["globaldata"] = globalData;
            ret["debug"] = (Json::Int)seed;

#ifdef _BOTZONE_ONLINE
            Json::FastWriter writer; // 在线评测的话能用就行……
#else
            Json::StyledWriter writer; // 本地调试这样好看 > <
#endif
            cout << writer.write(ret) << endl;
        }

        // 用于显示当前游戏状态，调试用。
        // 提交到平台后会被优化掉。
        inline void DebugPrint() const
        {
#ifndef _BOTZONE_ONLINE
            printf("回合号【%d】存活人数【%d】| 图例 产生器[G] 有玩家[0/1/2/3] 多个玩家[*] 大豆[o] 小豆[.]\n", turnID, aliveCount);
            for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                const Player &p = players[_];
                printf("[玩家%d(%d, %d)|力量%d|加成剩余回合%d|%s]\n",
                    _, p.row, p.col, p.strength, p.powerUpLeft, p.dead ? "死亡" : "存活");
            }
            putchar(' ');
            putchar(' ');
            for (int c = 0; c < width; c++)
                printf("  %d ", c);
            putchar('\n');
            for (int r = 0; r < height; r++)
            {
                putchar(' ');
                putchar(' ');
                for (int c = 0; c < width; c++)
                {
                    putchar(' ');
                    printf((fieldStatic[r][c] & wallNorth) ? "---" : "   ");
                }
                printf("\n%d ", r);
                for (int c = 0; c < width; c++)
                {
                    putchar((fieldStatic[r][c] & wallWest) ? '|' : ' ');
                    putchar(' ');
                    int hasPlayer = -1;
                    for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
                        if (fieldContent[r][c] & playerID2Mask[_])
                            if (hasPlayer == -1)
                                hasPlayer = _;
                            else
                                hasPlayer = 4;
                    if (hasPlayer == 4)
                        putchar('*');
                    else if (hasPlayer != -1)
                        putchar('0' + hasPlayer);
                    else if (fieldStatic[r][c] & generator)
                        putchar('G');
                    else if (fieldContent[r][c] & playerMask)
                        putchar('*');
                    else if (fieldContent[r][c] & smallFruit)
                        putchar('.');
                    else if (fieldContent[r][c] & largeFruit)
                        putchar('o');
                    else
                        putchar(' ');
                    putchar(' ');
                }
                putchar((fieldStatic[r][width - 1] & wallEast) ? '|' : ' ');
                putchar('\n');
            }
            putchar(' ');
            putchar(' ');
            for (int c = 0; c < width; c++)
            {
                putchar(' ');
                printf((fieldStatic[height - 1][c] & wallSouth) ? "---" : "   ");
            }
            putchar('\n');
#endif
        }

        Json::Value SerializeCurrentTurnChange()
        {
            Json::Value result;
            TurnStateTransfer &bt = backtrack[turnID - 1];
            for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
            {
                result["actions"][_] = bt.actions[_];
                result["strengthDelta"][_] = bt.strengthDelta[_];
                result["change"][_] = bt.change[_];
            }
            return result;
        }

        // 初始化游戏管理器
        GameField()
        {
            if (constructed)
                throw runtime_error("请不要再创建 GameField 对象了，整个程序中只应该有一个对象");
            constructed = true;

            newFruitsCount = 0;

            turnID = 0;
        }

        GameField(const GameField &b) : GameField() { }

        ~GameField() { constructed = false; /* 我们就要创建多次，哼！ */ }
    };

    bool GameField::constructed = false;
}

// 一些辅助程序
namespace Helpers
{

    int actionScore[5] = {};

    inline int RandBetween(int a, int b)
    {
        if (a > b)
            swap(a, b);
        return rand() % (b - a) + a;
    }

    void RandomPlay(Pacman::GameField &gameField, int myID)
    {
        int count = 0, myAct = -1;
        while (true)
        {
            // 对每个玩家生成随机的合法动作
            for (int i = 0; i < MAX_PLAYER_COUNT; i++)
            {
                if (gameField.players[i].dead)
                    continue;
                Pacman::Direction valid[5];
                int vCount = 0;
                for (Pacman::Direction d = Pacman::stay; d < 4; ++d)
                    if (gameField.ActionValid(i, d))
                        valid[vCount++] = d;
                gameField.actions[i] = valid[RandBetween(0, vCount)];
            }

            if (count == 0)
                myAct = gameField.actions[myID];

            // 演算一步局面变化
            // NextTurn返回true表示游戏没有结束
            bool hasNext = gameField.NextTurn();
            count++;

            if (!hasNext)
                break;
        }

        // 计算分数
        int rank2player[] = { 0, 1, 2, 3 };
        for (int j = 0; j < MAX_PLAYER_COUNT; j++)
            for (int k = 0; k < MAX_PLAYER_COUNT - j - 1; k++)
                if (gameField.players[rank2player[k]].strength > gameField.players[rank2player[k + 1]].strength)
                    swap(rank2player[k], rank2player[k + 1]);

        int scorebase = 1;
        if (rank2player[0] == myID)
            actionScore[myAct + 1]++;
        else
            for (int j = 1; j < MAX_PLAYER_COUNT; j++)
            {
                if (gameField.players[rank2player[j - 1]].strength < gameField.players[rank2player[j]].strength)
                    scorebase = j + 1;
                if (rank2player[j] == myID)
                {
                    actionScore[myAct + 1] += scorebase;
                    break;
                }
            }

        // 恢复游戏状态到最初（就是本回合）
        while (count-- > 0)
            gameField.PopState();
    }
}

namespace local_botzone
{
    /*Json::Value ret_bot_0, ret_bot_1, ret_bot_2, ret_bot_3;*/ //由bot传回的每回合数据，由 input_information写入
    
    // Bot 信息
    std::string strBotPath[MAX_PLAYER_COUNT];
    // Bot stdio 缓存区
    string strBotIO[MAX_PLAYER_COUNT];
    // 额外回合config
    bool AdditionalTurn[MAX_PLAYER_COUNT];
    int AdditionalTurnBotCount;

    // GameField &
    Pacman::GameField * gf;

    // Json root 开局初始化 (有一部分::(rootRequests[(Json::Value::UInt) 0])已经在gameField里边初始化了)
    void InitJsonRoot(string BotPath[MAX_PLAYER_COUNT]) {
        rootOthers["time_limit"] = Json::Value::Int(1000); // ms
        rootOthers["memory_limit"] = Json::Value::Int(256); // MB

        // data & globaldata
        for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
            std::fstream gdin; gdin.open(BotPath[i] + "gd.json", std::ios::in);
            if (!gdin) {
                gdin.close();
                gdin.open(BotPath[i] + "gd.json", std::ios::out);
                gdin.close();
                gdin.open(BotPath[i] + "gd.json", std::ios::in);
            }
            string tmpJsonStr;
            gdin >> tmpJsonStr;
            gdin.close();

            rootData[(Json::Value::UInt) i]["data"] = "";
            Json::Reader().parse(tmpJsonStr, rootData[(Json::Value::UInt) i]["globaldata"]);
        }

    }
    
    void PushVisualJson() {
        rootVisual[std::to_string(gf->turnID)] = rootRequests[(Json::Value::UInt) 0];
        std::string detailsStr;
        std::string strName[MAX_PLAYER_COUNT] = { "Red", "Green", "Blue", "Yellow" };
        detailsStr += "<div>Turn: " + std::to_string(gf->turnID) + "</div>";
        for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
            detailsStr += "<div>" + strName[i] + ": " + std::to_string(gf->players[i].strength)
                + "(" + std::to_string(gf->players[i].powerUpLeft) + ")" + (gf->players[i].dead ? "Dead" : "Alive") + "</div>";
        }
        rootVisual[std::to_string(gf->turnID)]["details"] = detailsStr;
        for (int i = 0; i < gf->height; ++i) {
            for (int j = 0; j < gf->width; ++j) {
                rootVisual[std::to_string(gf->turnID)]["content"][(Json::Value::UInt) i][(Json::Value::UInt) j] = gf->fieldContent[i][j];
            }
        }
    }

    // 保存log 及 globalData 及VisualLab可视化json
    void SaveData(string BotPath[MAX_PLAYER_COUNT], string logPath) {
        for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
            std::fstream gdout; gdout.open(BotPath[i] + "gd.json", std::ios::out);
            gdout << Json::FastWriter().write(rootData[(Json::Value::UInt) i]["globaldata"]);
            gdout.close();
        }
        std::fstream logout;
        time_t raw = time(0);
        struct tm * t = localtime(&raw);
        std::ostringstream sout;
        sout << std::setw(2) << std::setfill('0') << std::to_string(t->tm_mon + 1) << '-' << std::setw(2) << std::setfill('0') << std::to_string(t->tm_mday) << '_' << std::setw(2) << std::setfill('0') << std::to_string(t->tm_hour) << '-' << std::setw(2) << std::setfill('0') << std::to_string(t->tm_min) << '-' << std::setw(2) << std::setfill('0') << std::to_string(t->tm_sec);

        std::string logFilePath = logPath + sout.str() + ".requests-log.json";
        logout.open(logFilePath, std::ios::out);
        logout << Json::FastWriter().write(rootRequests);
        logout.close();
        std::string visualFilePath = logPath + sout.str() + ".visual-log.json";
        logout.open(visualFilePath, std::ios::out);
        logout << Json::FastWriter().write(rootVisual);
        logout.close();
    }

    Json::Value input_information(bool hasNext) {
        Json::Reader reader;
        Json::Value thisTurnRequests;
        string str[MAX_PLAYER_COUNT];

        for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
            if (!gf->players[i].dead && hasNext || AdditionalTurn[i] == true) {
                Json::Value ret;
                reader.parse(strBotIO[i], ret);
                if (!gf->players[i].dead) {
                    thisTurnRequests[std::to_string(i)]["action"] = ret["response"]["action"];
                }
                else {
                    AdditionalTurn[i] = false;
                }
                rootData[(Json::Value::UInt) i]["data"] = ret["data"];
                rootData[(Json::Value::UInt) i]["globaldata"] = ret["globaldata"];
            }

        }
        if (thisTurnRequests != Json::nullValue) {
            rootRequests.append(thisTurnRequests);
        }
        return thisTurnRequests;

        /*string a, b, c, d;*/
        //reader.parse(a, ret_bot_0);
        //reader.parse(b, ret_bot_1);
        //reader.parse(c, ret_bot_2);
        //reader.parse(d, ret_bot_3);

    }; // 未写，接收各个CPP传递通过的gameField.WriteOutPut()传递的数据，没看懂WriteOutPut把数据传到哪里去了，传出来的似乎以Jason::value保存的信息
       // Yoyo: 每个Bot都从stdin输入，输出到stdout; 进程管理时同步监听、改写stdio;

    void output_information(int i) {
        Json::Value botInput;
        botInput["data"] = rootData[(Json::Value::UInt) i]["data"];
        botInput["globaldata"] = rootData[(Json::Value::UInt) i]["globaldata"];
        botInput["memory_limit"] = rootOthers["memory_limit"];
        botInput["requests"] = rootRequests;
        // change id;
        botInput["requests"][(Json::Value::UInt) 0]["id"] = Json::Value::Int(i);
        botInput["time_limit"] = rootOthers["time_limit"];

        // push to String cache
        strBotIO[i] = Json::FastWriter().write(botInput);

        //需要在此函数中修改 information_of_field 的 information_of_field["request"]内容，information_of_field["request"][0]表示场地静态信息
        //information_of_field["request"][i]  (i > 0)表示各回合action, 将以下段代码修改即可
        /* 首先将information_of_field["request"]的size增加1,即在末尾增加一个JSON对象
        for (int i = 1; i < information_of_field["request"].size(); i++)
        {
        for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
        if (!players[_].dead)
        // 令information_of_field["request"][i][_]["actions"] = gameField.actions[_];  不知此句是否符合JSON规范
        }
        这是相同的更改；下面是各个bot不同的更改：
        传给每个bot的 information_of_field["request"][(Json::Value::UInt) 0]["id"] 应该与Bot的号码一致（0 ~ 3）
        */
    }; // 未写，向各个CPP传递数据，需要修改传递给各个bot的myID信息， int n 表示向哪个bot传递信息，各个bot信息只有myID不同, 从0-3， 需要参见ReadInput函数

    void main_for_game(Pacman::GameField &gameField, int repeatNow, int repeatAll)
    {
        AdditionalTurnBotCount = 0;
        for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
            if (AdditionalTurn[i]) {
                ++AdditionalTurnBotCount;
            }
        }
        bool hasNext = true;
        gf = &gameField; // Yoyo
        PushVisualJson();

        for (int turnID = 0;;++turnID)
        {
            for (int i = 0; i < MAX_PLAYER_COUNT; ++i)
            {
                if (!gf->players[i].dead && hasNext || AdditionalTurn[i] == true)
                {
                    output_information(i);
                }
                // 启动bot i, 并传递数据
                // 先放入缓存
            }

            // Bot 运行 Yoyo
            std::cout << "Repeat: " << repeatNow << '/' << repeatAll << "; ";
            std::cout << "Turn: " << std::setw(4) << turnID << "; Bot: ";
            for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
                bool run;
                if (!gf->players[i].dead && hasNext) {
                    std::cout << ((turnID & 1) ? '*' : '#');
                    run = true;
                }
                else if (AdditionalTurn[i] == true) {
                    std::cout << '@';
                    run = true;
                    /*AdditionalTurn[i] = false;*/
                    --AdditionalTurnBotCount;
                }
                else {
                    std::cout << "_";
                    run = false;
                }
                if (run) {
                    // change to
                    System::String^ filepath = gcnew System::String(strBotPath[i].c_str());
                    System::String^ iostring = gcnew System::String(strBotIO[i].c_str());

                    // run
                    BotZone::Bot^ bot = gcnew BotZone::Bot(filepath, iostring);
                    System::String^ output = bot->OutputGet();

                    // change back
                    MarshalString(output, strBotIO[i]);
                }

            }
            std::cout << '\r';
            // Bot 运行结束 Yoyo

            Json::Value ThisTurnResponse = input_information(hasNext); // 读取各bot内容到ret_bot_i...
                                 // 获取每个玩家的动作，直接略去叫嚣之类的东西
            for (int i = 0; i < MAX_PLAYER_COUNT; i++)
            {
                if (gf->players[i].dead)
                    continue;
                gf->actions[i] = (Pacman::Direction)ThisTurnResponse[std::to_string(i)]["action"].asInt();
            }

            

            // 往下进行游戏一步，演算一步局面变化
            // NextTurn返回true表示游戏没有结束
            if (hasNext) {
                hasNext = gf->NextTurn();
                // push data to visualLab json
                PushVisualJson();
            }
            else if (AdditionalTurnBotCount == 0) {
                return;
            }
        }
    }
}

void oneConfig(std::string & configPath) {
    // Load Config File
    std::ifstream config(configPath);
    std::string WorkingPath/* 工作路径， 所有其余的路径均为相对工作路径的相对路径*/, LogPath
        , MapPath, BotPath[MAX_PLAYER_COUNT]/* 全局信息文件路径 */;
    bool AdditionalTurn[MAX_PLAYER_COUNT];
    config >> WorkingPath >> MapPath >> LogPath;
    MapPath = WorkingPath + MapPath;
    LogPath = WorkingPath + LogPath;
    int repeat = 1;
    for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
        config >> BotPath[i] >> AdditionalTurn[i];
        BotPath[i] = WorkingPath + BotPath[i];
        local_botzone::strBotPath[i] = BotPath[i] + "bot";
    }
    config >> repeat;
    config.close();

    // 重复运行repeat次config
    for (int i = 0; i < repeat; ++i) {
        // 额外回合config重新载入
        for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
            local_botzone::AdditionalTurn[i] = AdditionalTurn[i];
        }
        // Start a round of game

        // Init && load globalDatas
        Pacman::GameField gameField; // 创建全局唯一的game_field
        gameField.ReadMap(MapPath);// 可以用这个函数来读入初始信息，只是丢弃了myID
                                   /* 改：丢弃data & globalData, 改名*/
        local_botzone::InitJsonRoot(BotPath);

        // 故事,就从这里开始了

        local_botzone::main_for_game(gameField, i, repeat);

        // Save Datas
        local_botzone::SaveData(BotPath, LogPath);
    }
}

int main(int argc, const char * argv[]) {
    // Load Config Paths
    std::string * configPaths;
    int n;
    if (argc >= 2) {
        n = argc - 1;
        configPaths = new std::string[n];
        for (int i = 0; i < n; ++i) {
            configPaths[i] = argv[i + 1];
        }
    }
    else { 
        std::cout << "输入config文件个数" << endl;
        std::cin >> n;
        configPaths = new std::string[n];
        std::cout << "输入" << n << "个config文件地址" << endl;
        for (int i = 0; i < n; ++i) {
            std::cin >> configPaths[i];
        }
    }

    for (int i = 0; i < n; ++i) {
        std::cout << endl;
        system("cls");
        std::cout << "config: " << i << '/' << n << ':' << configPaths[i] << endl;
        oneConfig(configPaths[i]);
    }

    // system("pause");
    return 0;
}






