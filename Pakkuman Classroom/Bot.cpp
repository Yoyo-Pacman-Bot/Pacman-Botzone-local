#include "Bot.h"
using namespace System;
using namespace System::Diagnostics;
using namespace System::IO;
using namespace System::Text;
BotZone::Bot::Bot(String ^ file, String ^ input)
{
    File = file;
    process = gcnew Process();
    process->StartInfo->UseShellExecute = false;
    process->StartInfo->FileName = File;
    process->StartInfo->CreateNoWindow = true;
    process->StartInfo->RedirectStandardOutput = true;
    Output = gcnew StringBuilder;
    process->OutputDataReceived += gcnew DataReceivedEventHandler(OutputHandler);
    process->StartInfo->RedirectStandardInput = true;
    process->Start();
    StreamWriter^ streamWriter = process->StandardInput;
    process->BeginOutputReadLine();
    streamWriter->WriteLine(input);
    streamWriter->Close();
    process->WaitForExit();
}

String^ BotZone::Bot::OutputGet()
{
    return Output->ToString();
}

void BotZone::Bot::OutputHandler(Object^ /*sendingProcess*/,
    DataReceivedEventArgs^ outLine)
{
    // Collect the sort command output.
    if (!String::IsNullOrEmpty(outLine->Data))
    {
        // Add the text to the collected output.
        Output->AppendFormat("{0}\n", outLine->Data);
    }
}


