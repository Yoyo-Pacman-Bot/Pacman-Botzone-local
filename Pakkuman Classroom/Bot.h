#pragma once
namespace BotZone {
    using namespace System;
    using namespace System::Diagnostics;
    using namespace System::IO;
    using namespace System::Text;
    /* Only One Instance At the same time!!!*/
    ref class Bot
    {
    public:
/* 0 */ Bot(String ^ file, String ^ input);
/* 1 */ String^ OutputGet();

        String^ File;
        int id;

        Process^ process;
        static StringBuilder^ Output = nullptr;

        static void OutputHandler(Object^ /*sendingProcess*/,
            DataReceivedEventArgs^ outLine);
    };
}