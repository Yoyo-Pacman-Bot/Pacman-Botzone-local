/**
* @file     ConfigMaker.cpp
* @author   Yoyo
* @date     2016/05/09 new version-1
* @description
*           
*/
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, const char * agrv[]) {
    string workingPath;
    string mapPath;
    string logPath;
    cout << "Working Path:\n";
    cin >> workingPath;
    cout << "Map Path\n";
    cin >> mapPath;
    cout << "Log Path\n";
    cin >> logPath;
    string botPath[4];
    string gdPath[4];
    bool botAdditionalTurn[4];
    for (int i = 0; i < 4; ++i) {
        cout << "botInfo " << i << endl;
        cin >> botPath[i] >> botAdditionalTurn[i];
    }
    int repeat;
    cout << "repeat:\n";
    cin >> repeat;
    for (int i = 1; i < argc; ++i) {
        string path(agrv[i]);
        string name = path.substr(path.find_last_of('\\') + 1, path.find_last_of('.') - path.find_last_of('\\') - 1);
        cout << name << endl;
        fstream fout((name + ".config").c_str(), ios::out);
        fout << workingPath << endl << mapPath + name + ".json" << endl << logPath << endl;
        for (int i = 0; i < 4; ++i) {
            fout << botPath[i] << endl << botAdditionalTurn[i] << endl;
        }
        fout << repeat << endl;
    }
    cin.get();
    return 0;
}

//***the end***